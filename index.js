var uuid = require('uuid');
var nJwt = require('njwt');
var readlineSync = require('readline-sync');

var userId = readlineSync.question("Enter userId: ");
var secret = readlineSync.question("Enter secret: ");
var claims = {
 "_id": userId
}

var jwt = nJwt.create(claims,secret,"HS256");
jwt.setExpiration(new Date().getTime() + 700*24*60*60*1000);
var token = jwt.compact();
console.log(token);